<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This is a library for CodeIgniter to access The Blue Alliance API.
 * 
 * @version 1.0 
 * @author Lea Fairbanks 
 * 
 * original library created by Chris Jelly from www.thebluealliance.com
 *
 */

/* *******************************************
 * tba_api_client_PHP5.php
 *
 * This page contains the client access library for PHP5 to The Blue Alliance API.
 * Version: 1, Release date: 02.09.08
 * Visit http://thebluealliance.net/ for details.
 *
 * ****************************************** */

class Thebluealliance {
    
    /*
     * not sure what version does, but it's passed with the api call...
     * it was '1' but I changed it to '1.0' without any problems
     */
    const VERSION = '1.0';

    /*
     * These are configuration defaults.
     */
    private $api_url = "http://thebluealliance.net/tbatv/api.php";
    private $api_key = '';
    private $cache = false;
    private $cache_table = 'tbacache';
    private $cache_time = '+1 days';
    private $curl = true;
    
    /*
     * a handle to codeigniter must be used inside of a library
     */
    private $ci;

    /**
     * Create the instance of our library.
     * 
     * Load config/thebluealliance and override the default settings
     * Overrides all settings with anything passed in $params
     * 
     * @param mixed $params An array containing config settings.
     */
    public function __construct($params = array()) {
        // get our handle to codeigniter
        $this->ci = & get_instance();

        // load the config file
        $this->ci->load->config('thebluealliance', TRUE);
        $config = $this->ci->config->item('thebluealliance');
        foreach ($config as $key => $value) {
            $this->$key = $value;
        }

        // override with constructor param array if necessary
        foreach ($params as $key => $value) {
            $this->$key = $value;
        }

        // if curl is enabled, make sure to load the library
        if ($this->curl) {
            $this->ci->load->library('curl');
        }
    }
    
    /**
     * check if the cache is enabled.
     * @return bool 
     */
    private function cache_enabled() {
        return $this->cache;
    }

    /**
     * check if the library is using curl.
     * @return bool 
     */
    private function curl_enabled() {
        return $this->curl;
    }

    /**
     * Send an API request to TBA.
     * 
     * @param string $method name of a TBA method to call from the API
     * @param mixed $arguments an array of arguements to pass to the API call
     * @return SimpleXMLElement the reply from the API 
     */
    public function tba_send_request($method, $arguments = array()) { //returns false on error, returns XML on success
        $api_key = $this->api_key;
        $api_url = $this->api_url;

        $version = Thebluealliance::VERSION;

        //basic method/key arguments (required)
        $argument_string = "version=$version&api_key=$api_key&method=$method";

        //loop through the array and make the arguments into a string
        foreach ($arguments as $key => $value) {
            $argument_string .= "&$key=" . urlencode($value);
        }

        $xml = '';
        $file = "{$api_url}?{$argument_string}";

        if ($this->cache_enabled()) {
            // check the cache for a cached entry
            // I didn't put this in a model incase someone wants to port it...
            $where_array = array(
                'host' => $api_url,
                'params' => md5($argument_string),
                'cached_until >' => date('Y-m-d H:i:s'),
            );
            $this->ci->db->select('content')->from($this->cache_table)->where($where_array);
            $query = $this->ci->db->get();

            if ($query->num_rows() > 0) {
                // got a result!
                $result = $query->row_array();
                $xml = new SimpleXMLElement($result['content']);
                $this->from_cache = true;
            } else {
                // there is no cache entry
                // so send an API request
                if ($this->curl_enabled()) {
                    $resp = $this->ci->curl->simple_get($file);
                    $xml = new SimpleXMLElement($resp);
                } else {
                    $xml = simplexml_load_file($file);
                }

                $this->from_cache = false;

                // cache the reply
                $insert_array = array(
                    'host' => $api_url,
                    'params' => md5($argument_string),
                    'content' => $xml->asXML(),
                    'cached_until' => date('Y-m-d H:i:s', strtotime($this->cache_time)),
                    'created' => date('Y-m-d H:i:s'),
                );
                $this->ci->db->insert($this->cache_table, $insert_array);
            }
        } else {
            // default behavior, no caching
            //create a simplexml element based on the arguments
            $this->from_cache = false;
            
            if ($this->curl_enabled()) {
                $resp = $this->ci->curl->simple_get($file);
                $xml = new SimpleXMLElement($resp);
            } else {
                $xml = simplexml_load_file($file);
            }
        }

        //if there is an error, echo it and die, otherwise return the XML elements
        if (count($xml->error) != 0) {
            die("Error: " . $xml->error->text);
            return false;
        }
            
        return $xml; //return as xml element (you can run this through make_array() to return a multi-dimensional associative array)
    }

    /**
     * makes an array out of a simplexmlelement object
     * 
     * @param SimpleXMLElement $xml
     * @return mixed An array of the result
     */
    public function make_array($xml) {
        //convert $xml into an array
        $array = array();

        foreach ($xml as $child) {
            $row = array();
            foreach ($child as $key => $value) {
                $row[$key] = (string) $value;
            }
            $array[] = $row;
        }

        return $array;
    }

    /**
     * send a get_teams request to the API
     * 
     * @param int $teamnumber
     * @param string $state
     * @return SimpleXMLElement|false
     */
    public function get_teams($teamnumber = NULL, $state = NULL) {
        return $this->tba_send_request("get_teams", array("teamnumber" => $teamnumber, "state" => $state));
    }

    /**
     * send a get_events request to the API
     * 
     * $week can be in the following format: 
     *      0   - pre-ship
     *      10  - week 1
     *      20  - week 2
     *      30  - week 3
     *      40  - week 4
     *      50  - week 5
     *      60  - week 6
     *      90  - world championship - divisions
     *      91  - world championship - finals
     *      100 - off-season events
     * 
     * @param int $eventid
     * @param int|string $year
     * @param int $week 
     * @return SimpleXMLElement|false
     */    
    public function get_events($eventid = NULL, $year = NULL, $week = NULL) {
        $valid_weeks = array(0, 10, 20, 30, 40, 50, 60, 90, 91, 100, '0', '10', '20', '30', '40', '50', '60', '90', '91', '100', '', NULL);
        if(array_search($week, $valid_weeks) !== false) {
            return $this->tba_send_request("get_events", array("eventid" => $eventid, "year" => $year, "week" => $week));
        } else return false;
        
    }

    /**
     * send a get_matches request to the API
     * 
     * $complevel can be in the following format: 
     *      'qualifications'
     *      'quarters'
     *      'semis'
     *      'finals'
     * 
     * @param int $teamnumber
     * @param int $eventid
     * @param int $matchid
     * @param string $complevel
     * @return SimpleXMLElement|false 
     */
    public function get_matches($teamnumber = NULL, $eventid = NULL, $matchid = NULL, $complevel = NULL) {
        $valid_complevels = array('qualifications', 'quarters', 'semis', 'finals', '', NULL);
        if(array_search($complevel, $valid_complevels) !== false) {
            return $this->tba_send_request("get_matches", array("eventid" => $eventid, "teamnumber" => $teamnumber, "matchid" => $matchid, "complevel" => $complevel));
        } else return false;
    }

    /**
     * send a get_matches request to the API
     * 
     * @param int $eventid
     * @param int $teamnumber
     * @return SimpleXMLElement|false 
     */
    public function get_attendance($eventid = NULL, $teamnumber = NULL) {
        return $this->tba_send_request("get_attendance", array("eventid" => $eventid, "teamnumber" => $teamnumber));
    }

    /**
     * send a get_official_record request to the API
     * 
     * @param int $teamnumber
     * @param int $eventid
     * @param int $year
     * @return SimpleXMLElement|false 
     */
    public function get_official_record($teamnumber, $eventid = NULL, $year = NULL) {
        return $this->tba_send_request("get_official_record", array("teamnumber" => $teamnumber, "eventid" => $eventid, "year" => $year));
    }

    /**
     * send a get_elim_sets request to the API
     * 
     * $noun can be in the following format: 
     *      'quarterfinalists'
     *      'semifinalists'
     *      'finalists'
     * 
     * @param int $eventid
     * @param int $noun
     * @return SimpleXMLElement|false 
     */
    public function get_elim_sets($eventid, $noun) {
        $valid_nouns = array('quarterfinalists', 'semifinalists', 'finalists', '', NULL);
        if(array_search($noun, $valid_nouns) !== false) {
            return $this->tba_send_request("get_elim_sets", array("eventid" => $eventid, "noun" => $noun));
        } else return false;
    }

    /**
     * No idea what this does, leaving it in for legacy purposes
     * 
     * @param mixed $text
     * @return SimpleXMLElement|false 
     */
    public function throw_error($text = NULL) { //this exists solely for debugging purposes
        return $this->tba_send_request("throw_error", array("text" => $text));
    }

    /**
     * purge the cache of old entries
     * 
     * @param bool $expired_only
     */
    public function purge_cache($expired_only = true) {
        if ($expired_only) {
            $this->ci->db->delete($this->cache_table, "cached_until < '" . date('Y-m-d H:i:s') . "'");
        } else {
            $this->ci->db->query("DELETE FROM `" . $this->cache_table . "` WHERE 1=1");
        }
    }

    /**
     * check if the previous API call was taken from the 
     * cache or retrieved from the API
     * 
     * @return bool
     */
    public function from_cache() {
        return $this->from_cache;
    }

}