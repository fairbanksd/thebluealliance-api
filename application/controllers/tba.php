<?

/**
 * This controller demonstrates basic use of the library.
 * It is left largely unmodified from the original by Chris Jelly
 */
class Tba extends CI_Controller {

    public function team($num = 706) {

        $this->load->library('thebluealliance');

        $teamnumber = $num;

        //get team
        $teams = $this->thebluealliance->make_array($this->thebluealliance->get_teams($teamnumber));

        //show team data
        $overall_record = $this->thebluealliance->make_array($this->thebluealliance->get_official_record($teamnumber));

        //introduction and basic information
        echo "<h1>History of Team $teamnumber</h1>";
        echo "<p>Below is some information about our team, {$teams[0]["informalname"]}, from {$teams[0]["city"]}, {$teams[0]["state"]}. Our overall record at official events is {$overall_record[0]["string"]}.</p>";

        echo "<table id=\"list_events\">";
        echo "<tr><th>Year</th><th>Event</th><th>Record</th><th>Wins</th></tr>";
        $future = "";
        $team_events = $this->thebluealliance->make_array($this->thebluealliance->get_attendance(NULL, $teamnumber));

        foreach ($team_events as $attendance) {
            echo "<tr>";
            //get event record
            $event = $this->thebluealliance->make_array($this->thebluealliance->get_events($attendance["eventid"]));

            //show the user some data about this event
            $official_text = '';
            if ($event[0]["official"] == false) {
                $official_text = "*";
            }

            //get team's record at event
            $record = $this->thebluealliance->make_array($this->thebluealliance->get_official_record($teamnumber, $event[0]["eventid"]));

            //show data to user
            if ($record[0]["sum"] != 0) {
                $percent = round(($record[0]["win"] / $record[0]["sum"]) * 100) . "%";
            } else {
                $percent = "0%";
            }

            echo "<td><a href=\"http://www.thebluealliance.net/tbatv/eventlist.php?year={$event[0]["year"]}\">{$event[0]["year"]}</a></td>";
            echo "<td><a href=\"{$event[0]['tba_link']}\">{$event[0]['name']}</a>$official_text</td>";
            echo "<td>({$record[0]["string"]})</td>";
            echo "<td>Won {$percent}</td>";
            echo "</tr>";
        }

        echo "</table>";
    }

    public function purge($expired_only=true) {
        $this->load->library('thebluealliance');
        $this->thebluealliance->purge_cache($expired_only);
    }
    
    public function doc() {
        $this->load->view('doc');
    }

}
