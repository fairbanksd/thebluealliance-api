<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

    /*
        The API url used to talk to The Blue Alliance
        You shouldn't need to change this!
    */
    $config['api_url'] = "http://thebluealliance.net/tbatv/api.php";

    /*
        The API key assigned to you by The Blue Alliance.
        You can get one by emailing contact+apikey@thebluealliance.net with your intended use.
        They'll reply with the API key you can use.

        If you leave this blank, you'll still recieve valid information
        But it will be limited to a few years ago.  You can test it without getting an API key!
    */
    $config['api_key'] = '';

    /*
        The cache configuration settings are if you'd like to use the MySQL caching feature.
        It requires that the database library is autoloaded by CodeIgniter

        cache may be true/false
        cache_table is the name of the mysql table you wish to cache to
        cache_time is the amount of time the xml should be cached, it must be in a format suitable for strtotime
        
        examples:
            $config['cache_time'] = '+1 days'
            $config['cache_time'] = '+1 weeks'
            $config['cache_time'] = '+1 months'
            $config['cache_time'] = '+4 days'
            $config['cache_time'] = '+3 minutes +30 seconds'
            $config['cache_time'] = '+3 years -2 years -50 weeks' // er... you get the idea...
    */
    $config['cache'] = true;
    $config['cache_table'] = 'tbacache';
    $config['cache_time'] = '+3 minutes';

    /*
    use cURL to get the XML file
    may be true/false
    false will use filegetcontents over http, which may not be supported by your hosting provider
    but then again, curl might not be supported either...
    */
    $config['curl'] = true;

