<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        
    </head>
    <body>
        <h1>The Blue Alliance API library</h1>
        <h3>For CodeIgniter</h3>
        This library is for CodeIgniter and was created for the FIRST Alumni website project located on bitbucket <a href="https://bitbucket.org/fairbanksd/firstalumni">here</a>
        <br/>
        This library was originally developed by Chris Jelly of FRC 177 (Bobcat Robotics) and The Blue Alliance and released on 02/09/2008
        <br/><br/>
        It was ported to CodeIgniter on 5/13/2012 by Lea Fairbanks, a FIRST Alumni of Team 706 (The Cyberhawks) and included a couple major improvements.
        <ul>
            <li>Ability to cache responses to MySQL database [for an amount of time specified in the config]</li>
            <li>Ability to use cURL to fetch the XML documents.</li>
        </ul>
        Usage: 
        <ol>
            <li>Include the files into CodeIgniter following the directory structure provided</li>
            <li>Import the .sql file located in the application/sql folder into your database</li>
            <li>Set the configuration settings in application/config/thebluealliance.php</li>
        </ol>
    </body>
</html>

      