# The Blue Alliance API library

### For CodeIgniter

This library is for CodeIgniter and was created for the FIRST Alumni website
project located on bitbucket
[here](https://bitbucket.org/fairbanksd/firstalumni)

This library was originally developed by Chris Jelly of FRC 177 (Bobcat
Robotics) and The Blue Alliance and released on 02/09/2008

  
It was ported to CodeIgniter on 5/13/2012 by Lea Fairbanks, a FIRST Alumni of
Team 706 (The Cyberhawks) and included a couple major improvements.

  * Ability to cache responses to MySQL database [for an amount of time specified in the config]
  * Ability to use cURL to fetch the XML documents.
  
  
  
Usage:

  1. Include the files into CodeIgniter following the directory structure provided
  2. Import the .sql file located in the application/sql folder into your database
  3. Set the configuration settings in application/config/thebluealliance.php
